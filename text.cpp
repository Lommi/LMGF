#include "text.h"

Font*
Text_loadFont(char *path, int size)
{
    Font *font = 0;
    font = TTF_OpenFont(AppendString(ASSETS_PATH, path), size);
    if (!font)
    {
        PRINT("failed to load font %s\n SDL_ttf Error: %s\n", path, TTF_GetError());
        return 0;
    }
    return font;
}

int
Text_create(Texture *texture, char *text,
    Font *font, SDL_Color color, unsigned int wrapWidth)
{
    if (texture->id != 0)
    {
        Draw_freeTexture(texture);
    }

    if (font == nullptr){ PRINT("App_createText: font = nullptr\n"); return 1; }
    if (text == nullptr){ PRINT("App_createText: text = nullptr\n"); return 2; }

    SDL_Surface *textSurface;

    if (wrapWidth > 0)
    {
        textSurface = TTF_RenderText_Blended_Wrapped(font, text, color, (int)wrapWidth);
    }
    else
    {
        textSurface = TTF_RenderText_Blended(font, text, color);
    }

    if (!textSurface)
    {
        PRINT("App_createText: Text surface creation failed");
        return 3;
    }

    glGenTextures(1, &texture->id);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textSurface->w, textSurface->h,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, textSurface->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);

    texture->width  = textSurface->w;
    texture->height = textSurface->h;

    SDL_FreeSurface(textSurface);

    return 0;
}
