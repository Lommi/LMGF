#pragma once

#include "defs.h"

bool
Utils_writeFile(char *org, char *gameName, char *fileName, void *data, int len);

bool
Utils_readFile(char *org, char *gameName, char *fileName, void *output, int len);

#ifndef MOBILE
char *
Utils_readFile(char *fileName);
#endif

inline vec2
VecSubs(vec2 left, vec2 right);

inline int
VecLen(vec2 vec);

inline int
VecDotProduct(vec2 vec1, vec2 vec2);

inline float
VecAngleBetween(vec2 vec1, vec2 vec2);

inline vec2
VecAdd(vec2 *a, vec2 *b);

inline fvec2
VecAdd(fvec2 *a, fvec2 *b);

inline fvec2
VecSubs(fvec2 left, fvec2 right);

inline char *
AppendString(char *s1, char *s2);

inline char *
IntToString(char *str, int number);

inline float
Approach(float goal, float current, float s);

inline int
AABB(float *a, float *b);

inline int
AABB(uint *a, uint *b);

inline int
AABB(rect *a, rect *b);

inline int
AABB(frect *a, frect *b);

inline bool
AABBPoints(int x, int y, int rectX, int rectY, int rectW, int rectH);

inline vec2
VecSubs(vec2 left, vec2 right)
{
    vec2 ret;
    ret.x = left.x - right.x;
    ret.y = left.y - right.y;
    return ret;
}

static inline int
VecLen(vec2 vec)
{
    return (int)sqrtf((float)(vec.x * vec.x + vec.y * vec.y));
}

static inline vec2
VecAdd(vec2 *a, vec2 *b)
{
    vec2 add;
    add.x = a->x += b->x;
    add.y = a->y += b->y;
    return add;
}

static inline fvec2
VecAdd(fvec2 *a, fvec2 *b)
{
    fvec2 add;
    add.x = a->x += b->x;
    add.y = a->y += b->y;
    return add;
}

static inline fvec2
VecSubs(fvec2 left, fvec2 right)
{
    fvec2 ret = { left.x - right.x, left.y - right.y };
    return ret;
}
static inline int
VecDotProduct(vec2 vec1, vec2 vec2)
{
    return vec1.x * vec2.x + vec1.y * vec2.y;
}
static inline float
VecAngleBetween(vec2 vec1, vec2 vec2)
{
    return acos((float)VecDotProduct(vec1, vec2) / (VecLen(vec1) * VecLen(vec2)));
}

inline char *
ConvertIntToString(char *buffer, int bufferlength,  int index)
{
    snprintf(buffer, bufferlength, "%i", index);
    return buffer;
}

inline float
Approach(float goal, float current, float s)
{
    float difference = goal - current;

    if (difference > s)
        return current + s;
    if (difference < -s)
        return current - s;

    return goal;
}

inline char *
AppendString(char *s1, char *s2)
{
    char *str = (char*)malloc((strlen(s1) + strlen(s2) + 1) * sizeof(char));
    strcpy(str, s1);
    strcat(str, s2);
    return str;
}

inline char *
IntToString(char *str, int i)
{
    sprintf(str, "%d", i);
    return str;
}

inline int
AABB(uint *a, uint *b)
{
    if ((a[0] < b[0] + b[2] &&
         a[0] + a[2] > b[0] &&
         a[1] < b[1] + b[3] &&
         a[1] + a[3] > b[1]) ||
        (b[0] < a[0] + a[2] &&
         b[0] + b[2] > a[0] &&
         b[1] < a[1] + a[3] &&
         b[1] + b[3] > a[1]))return 1;
    return 0;
}

inline int
AABB(float *a, float *b)
{
    if ((a[0] < b[0] + b[2] &&
        a[0] + a[2] > b[0] &&
        a[1] < b[1] + b[3] &&
        a[1] + a[3] > b[1]) ||
        (b[0] < a[0] + a[2] &&
            b[0] + b[2] > a[0] &&
            b[1] < a[1] + a[3] &&
            b[1] + b[3] > a[1]))return 1;
    return 0;
}

inline int
AABB(rect *a, rect *b)
{
    if ((a->x < b->x + b->w &&
        a->x + a->w > b->x &&
        a->y < b->y + b->h &&
        a->y + a->h > b->y) ||
        (b->x < a->x + a->w &&
            b->x + b->w > a->x &&
            b->y < a->y + a->h &&
            b->y + b->h > a->y))return 1;
    return 0;
}

inline int
AABB(frect *a, frect *b)
{
    if ((a->x < b->x + b->w &&
        a->x + a->w > b->x &&
        a->y < b->y + b->h &&
        a->y + a->h > b->y) ||
        (b->x < a->x + a->w &&
            b->x + b->w > a->x &&
            b->y < a->y + a->h &&
            b->y + b->h > a->y))return 1;
    return 0;
}

inline bool
AABBPoints(int x, int y, int rectX, int rectY, int rectW, int rectH)
{
    if (x <= rectW &&
        x >= rectX &&
        y <= rectH &&
        y >= rectY) return true;
    else return false;
}
