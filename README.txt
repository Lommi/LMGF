Lommi Mobile Game Framework
Created by Juho Lommi

Description:
Lommi Mobile Game Framework (LMGF) is a collection of C/C++ scripts for mobile game development
purposes.
Uses mainly SDL2 and OpenGL ES.

Over the course of a few game projects I noticed there are features
that are so generic they should be fletched into their own respective systems
that could be used in any game project. For example screen scaling,
gamepad support, mobile input,
math algorithms (A*), saving, loading, networking, Textures, Shaders, Sound and
music are examples of theses kind of features.

Adding of features should commence when during game development one realises
the feature at hand would work better as a more generic system, at which
point the feature should be transported to the LMGF. After including the feature
to LMGF the implementation should be used in the game project and see how
well it manages to do the job.