#include "audio.h"

SoundClip*
Audio_loadSound(char *path)
{
    SoundClip *sound = 0;
    sound = Mix_LoadWAV(AppendString(ASSETS_PATH, path));
    if (!sound)
    {
        PRINT("failed to load sound %s\n", path);
        return 0;
    }
    return sound;
}

void
Audio_setVolume(int channel, int value)
{
    if (value > MAX_VOLUME)
        value = MAX_VOLUME;
    if (value < 0)
        value = 0;

    if (channel > NUM_AUDIO_CHANNELS)
        channel = NUM_AUDIO_CHANNELS - 1;
    if (channel < 0)
        channel = 0;

    Mix_Volume(channel, value);
}

void
Audio_setAllVolume(int value)
{
    for (int i = 0; i < NUM_AUDIO_CHANNELS; ++i)
        Mix_Volume(i, value);
}

void
Audio_playSound(SoundClip *sound)
{
    Mix_PlayChannel(-1, sound, 0);
}

void
Audio_playSound(SoundClip *sound, int loops) // 0 = play once, -1 = loop forever
{
    Mix_PlayChannel(-1, sound, loops);
}

void
Audio_playSound(SoundClip *sound, int channel, int loops)
{
    Mix_PlayChannel(channel, sound, loops);
}

void
Audio_playMusic(MusicClip *music, int loops)
{
    Mix_PlayMusic(music, loops);
}
