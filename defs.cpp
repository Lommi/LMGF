#include "defs.h"

char *LMGF_PATH;
char *ASSETS_PATH;

void
Defs_initFolderPaths(const char *lmgfFolderPath, const char *assetsFolderPath)
{
    LMGF_PATH = (char*)malloc(strlen(lmgfFolderPath) * sizeof(char));
    ASSETS_PATH = (char*)malloc(strlen(assetsFolderPath) * sizeof(char));
    strcpy(LMGF_PATH, lmgfFolderPath);
    strcpy(ASSETS_PATH, assetsFolderPath);
}
