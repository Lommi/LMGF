#pragma once

#include "defs.h"

#ifdef MOBILE
#include "deps/SDL2-2.0.8/include/SDL.h"
#else
#include "deps/SDL2/SDL.h"
#endif

typedef struct Window Window;

void
Window_setResolution(Window *window, uint w, uint h);

void
Window_setViewport(Window *window, uint x, uint y, uint w, uint h);

void
Window_setColor(Window *window, float r, float g, float b, float a);

inline void
Window_translateScreenCoordinatesByViewport(uint *x, uint *y,
   int trueX, int trueY, Window *window);

struct Window
{
    SDL_Window *sdlWindow;
    uint       width;
    uint       height;
    vec2       resolution;
    float      viewport[4];
    float      viewportScale;
    float      r, g, b, a;
};


