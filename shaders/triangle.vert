#version 120
precision lowp float;
attribute vec2 pos;
attribute vec4 in_color;
varying vec4 color;
uniform mat4 p;
void main(void)
{
   gl_Position = p * vec4(pos.x, pos.y, 0.0, 1.0);
   color = in_color;
}
