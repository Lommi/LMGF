#version 120
precision highp float;
attribute vec2 pos;
attribute vec2 in_tex_coord;
varying vec2 tex_coord;
uniform mat4 p;
void main(void)
{
   gl_Position = p * vec4(pos.x, pos.y, 0.0, 1.0);
   tex_coord = in_tex_coord;
}
