#version 120
precision lowp float;
varying vec4 color;
void main(void)
{
   gl_FragColor = color;
}
