#include "window.h"

void
Window_setResolution(Window *window, uint w, uint h)
{
    window->width = w;
    window->height = h;
    window->resolution.x = window->width;
    window->resolution.y = window->height;
}

void
Window_setViewport(Window *window, uint x, uint y, uint w, uint h)
{
    window->viewport[0] = (float)x;
    window->viewport[1] = (float)y;
    window->viewport[2] = (float)w;
    window->viewport[3] = (float)h;
}

void
Window_setColor(Window *window, float r, float g, float b, float a)
{
    window->r = r;
    window->g = g;
    window->b = b;
    window->a = a;
}

inline void
Window_translateScreenCoordinatesByViewport(uint *x, uint *y,
    int trueX, int trueY, Window *window)
{
    *x = (uint)(trueX * window->viewportScale + window->viewport[0]);
    *y = (uint)(trueY * window->viewportScale + window->viewport[1]);
}
