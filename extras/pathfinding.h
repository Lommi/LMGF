#pragma once

#include "defs.h"

using namespace std;

typedef struct Grid Grid;
typedef struct Node Node;

bool gridInit(Grid *grid, int width, int height);
bool findPath(Grid *grid);

struct Grid
{
    int width;
    int height;

    Node *nodes = nullptr;
    Node *nodeStart = nullptr;
    Node *nodeEnd = nullptr;
    vector<Node*> path;
};

struct Node
{
    bool obstacle = false;
    bool visited = false;
    float globalGoal;
    float localGoal;
    unsigned int x;
    unsigned int y;
    unsigned int type;
    vector<Node*> neighbours;
    Node *parent;
};

