#pragma once

#include <errno.h>
#include <time.h>

#include "defs.h"

#ifdef MOBILE
#include <unistd.h>
#include "deps/SDL2-2.0.8/include/SDL.h"
#include "deps/SDL2-2.0.8/include/SDL_opengles.h"
#include "deps/SDL2_mixer-2.0.2/SDL_mixer.h"
#include "deps/SDL2_image-2.0.2/SDL_image.h"
#include "deps/SDL2_ttf-2.0.14/SDL_ttf.h"
#else
#include <stdio.h>
#include <Windows.h>
#include "deps/SDL2/SDL.h"
#include "deps/SDL2/SDL_image.h"
#include "deps/SDL2/SDL_ttf.h"
#include "deps/SDL2/SDL_mixer.h"
#include "deps/GL/glew.h"
#include "deps/SDL2/SDL_opengl.h"
#endif

#include "utils.h"
#include "draw.h"
#include "text.h"
#include "audio.h"

typedef struct Window           Window;
typedef struct Clock            Clock;
typedef struct App              App;
typedef struct TouchFinger      TouchFinger;
typedef struct MouseInput       MouseInput;

static SDL_Color colorWhite       = {0xFF, 0xFF, 0xFF, 0xFF};
static SDL_Color colorBlack       = {0x00, 0x00, 0x00, 0xFF};
static SDL_Color colorRed         = {0x00, 0x00, 0xFF, 0xFF};
static SDL_Color colorGreen       = {0x00, 0xFF, 0x00, 0xFF};
static SDL_Color colorBlue        = {0xFF, 0x00, 0x00, 0xFF};
static SDL_Color colorYellow      = {0x00, 0xFF, 0xFF, 0xFF};
static SDL_Color colorPurple      = {0xFF, 0x00, 0xFF, 0xFF};
static SDL_Color colorOrange      = {0x00, 0xA5, 0xFF, 0xFF};
static SDL_Color colorAqua        = {0xFF, 0xFF, 0x00, 0xFF};

static const uint8_t *KEYSTATE;

#ifndef MOBILE
void usleep(DWORD waitTime);
#endif

enum MouseButtons
{
    MB_LEFT = 0,
    NUM_MOUSEBUTTONS
};

void
App_mainloop(App *app);

int
App_init(App *app, const char *window_name, const char *lmgfFolderPath, const char *assetsFolderPath); // Return 0 on success

void
App_input(App *app);

void
App_update(App *app);

void
App_render(App *app);

void
App_shutDown(App *app);

void
App_calculateWindowViewport(float *viewport, Window *window);

inline void
App_translateScreenCoordinatesByWindowViewport(uint *x, uint *y,
   int real_x, int real_y, Window *window);

void
App_setFps(App *app, int amount);

double
App_getTimeSinceAppStarted(App *app);

void
App_setWindowColor(Window *window, float r, float g, float b, float a);

void
Clock_tick(Clock *clock);

#ifdef MOBILE
#else
void CalculateMousePosition(App *app, SDL_MouseButtonEvent button);
#endif

struct Window
{
    SDL_Window *sdlWindow;
    uint       width;
    uint       height;
    vec2       resolution;
    float      viewport[4];
    float      viewportScale;
    float      color_r;
    float      color_g;
    float      color_b;
    float      color_a;
};

struct Clock
{
    double delta;      // Format: seconds
    double lastTick;
    double fps;
    bool32 limitFps;
    uint   targetFps;
};

struct TouchFinger
{
    bool32 fingerDown;
    bool32 downThisFrame;
    bool32 upThisFrame;
    uvec2  lastPosition;
    uvec2  position;
};

struct MouseInput
{
    bool32 downThisFrame;
    bool32 upThisFrame;
    uvec2 lastPosition;
    uvec2 position;
};

struct App
{
    unsigned int                fingerIndex;
    time_t                      appStartTime;
    volatile bool32	            running;
    Window				        window;
    Clock				        clock;
    SDL_GLContext		        glContext;
    vec2                        trueInput;
    TouchFinger                 touchInputs[NUM_TOUCH_FINGERS];
    MouseInput                  mouseInputs[NUM_MOUSEBUTTONS];
    SDL_Keycode                 keyboardInput;

    void                        (*Render)();
    void                        (*PreRender)();
    void                        (*PostRender)();
    void                        (*FingerDown)();
    void                        (*FingerUp)();
    void                        (*FingerMotion)();
    void                        (*KeyDown)();
    void                        (*KeyUp)();
    void                        (*Update)();
};
