#include "defs.h"
#include "pathfinding.h"

bool gridInit(Grid *grid, int width, int height)
{
    grid->width = width;
    grid->height = height;

    grid->nodes = new Node[width * height];

    int nodeIndex = 0;

	for (int x = 0; x < width; ++x) {
        for (int y = 0; y < height; ++y) {
            nodeIndex = y * width + x;
            grid->nodes[nodeIndex].x = x;
            grid->nodes[nodeIndex].y = y;
            grid->nodes[nodeIndex].obstacle = false;
            grid->nodes[nodeIndex].parent = nullptr;
            grid->nodes[nodeIndex].visited = false;
            grid->nodes[nodeIndex].type = 0;
        }
    }

	for (int x = 0; x < width; ++x) {
        for (int y = 0; y < height; ++y) {
            nodeIndex = y * width + x;
            if (y > 0)
                grid->nodes[nodeIndex].neighbours.push_back(
                        &grid->nodes[(y - 1) * width + (x + 0)]);
            if (y < height - 1)
                grid->nodes[nodeIndex].neighbours.push_back(
                        &grid->nodes[(y + 1) * width + (x + 0)]);
            if (x > 0)
                grid->nodes[nodeIndex].neighbours.push_back(
                        &grid->nodes[(y + 0) * width + (x - 1)]);
            if (x < width - 1)
                grid->nodes[nodeIndex].neighbours.push_back(
                        &grid->nodes[(y + 0) * width + (x + 1)]);

            if (y > 0 && x > 0)
                grid->nodes[nodeIndex].neighbours.push_back(
                        &grid->nodes[(y - 1) * width + (x - 1)]);
            if (y < height - 1 && x > 0)
                grid->nodes[nodeIndex].neighbours.push_back(
                        &grid->nodes[(y + 1) * width + (x - 1)]);
            if (y > 0 && x < width - 1)
                grid->nodes[nodeIndex].neighbours.push_back(
                        &grid->nodes[(y - 1) * width + (x + 1)]);
            if (y < height - 1 && x < width - 1)
                grid->nodes[nodeIndex].neighbours.push_back(
                        &grid->nodes[(y + 1) * width + (x + 1)]);
        }
    }

	grid->nodeStart = &grid->nodes[(height / 2) * width + 1];
	grid->nodeEnd = &grid->nodes[(height / 2) * width + width-2];

	return true;
}

bool findPath(Grid *grid)
{
    int width = grid->width;
    int height = grid->height;

	for (int x = 0; x < width; ++x) {
        for (int y = 0; y < height; ++y) {
            grid->nodes[y * width + x].visited = false;
            grid->nodes[y * width + x].globalGoal = INFINITY;
            grid->nodes[y * width + x].localGoal = INFINITY;
            grid->nodes[y * width + x].parent = nullptr;
        }
    }

	auto distance = [](Node* a, Node* b) // For convenience
	{
		return (sqrtf((float)((a->x - b->x)*(a->x - b->x) + (a->y - b->y)*(a->y - b->y))));
	};

	auto heuristic = [distance](Node* a, Node* b) // So we can experiment with heuristic
	{
		return distance(a, b);
	};

    Node *nodeCurrent = grid->nodeStart;
    grid->nodeStart->localGoal = 0.0f;
    grid->nodeStart->globalGoal = heuristic(grid->nodeStart, grid->nodeEnd);

    list<Node*> listNotTestedNodes;
    listNotTestedNodes.push_back(grid->nodeStart);

    while (!listNotTestedNodes.empty() && nodeCurrent != grid->nodeEnd)
    {
        listNotTestedNodes.sort([](const Node* lhs, const Node* rhs){ return lhs->globalGoal < rhs->globalGoal; } );
        while(!listNotTestedNodes.empty() && listNotTestedNodes.front()->visited)
            listNotTestedNodes.pop_front();

        if (listNotTestedNodes.empty())
            break;

        nodeCurrent = listNotTestedNodes.front();
        nodeCurrent->visited = true;

        for (auto nodeNeighbour : nodeCurrent->neighbours)
        {
            if (!nodeNeighbour->visited && nodeNeighbour->obstacle == 0)
                listNotTestedNodes.push_back(nodeNeighbour);

            float possiblyLowerGoal = nodeCurrent->localGoal + distance(nodeCurrent, nodeNeighbour);

            if (possiblyLowerGoal < nodeNeighbour->localGoal)
            {
                nodeNeighbour->parent = nodeCurrent;
                nodeNeighbour->localGoal = possiblyLowerGoal;

                nodeNeighbour->globalGoal = nodeNeighbour->localGoal + heuristic(nodeNeighbour, grid->nodeEnd);
            }
        }
    }
    if (grid->nodeEnd == nullptr)
        return false;
    else
    {
        grid->path.clear();
        Node *p = grid->nodeEnd;
        while (p->parent != nullptr)
        {
            if (!p->obstacle)
                grid->path.push_back(p);

            p = p->parent;
        }
    }

    return true;
}


