#include "../LGH/app.h"

void Init();
void Update();
void Render();
void KeyDown();
void KeyUp();
void FingerDown();
void FingerUp();
void FingerMotion();

App app;

enum GameState
{
    GS_MENU = 0,
    GS_GAME = 1,
    GS_PAUSE = 2
};

GameState gameState;

int main(int argc, char **argv)
{
    App_init(&app, "Template");

    Init();

    App_mainloop(&app);

    return 0;
}

void
Init()
{
    gameState = GS_MENU;

    app.Update = Update;
    app.Render = Render;
    app.FingerUp = FingerUp;
    app.FingerDown = FingerDown;
    app.FingerMotion = FingerMotion;
    app.KeyDown = KeyDown;
    app.KeyUp = KeyUp;
}

void
Render()
{
}

void
Update()
{
}

void
KeyDown()
{
}

void
KeyUp()
{
}

void
FingerUp()
{
}

void
FingerDown()
{
}

void
FingerMotion()
{
}
