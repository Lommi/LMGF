#include "app.h"

void
App_mainloop(App *app)
{
    app->running = 1;

    while (app->running)
    {
#ifndef MOBILE
        Clock_tick(&app->clock);
#endif
        App_input(app);
        App_update(app);
        App_render(app);
    }
}

int
App_init(App *app, const char *window_name, const char *lmgfFolderPath, const char *assetsFolderPath)
{
    Defs_initFolderPaths(lmgfFolderPath, assetsFolderPath);

    app->appStartTime = time(0);

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        PRINT("SDL_Init() error: %s\n", SDL_GetError());
        return 1;
    }

    if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 1024) < 0)
    {
        PRINT("Failed to open audio. Mix_OpenAudio() error: %s\n", Mix_GetError());
        return 2;
    }

    Mix_AllocateChannels(NUM_AUDIO_CHANNELS);
    for (int i = 0; i < NUM_AUDIO_CHANNELS; ++i)
        Mix_Volume(i, MAX_VOLUME);

    if (TTF_Init() == -1)
    {
        PRINT("TTF_Init() error: %s\n", TTF_GetError());
        return 3;
    }

    if (IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG) < 0)
    {
        PRINT("IMG_Init error: %s\n", IMG_GetError());
        return 4;
    }

    srand((uint)time(0));

    for (int i = 0; i < NUM_TOUCH_FINGERS; ++i)
    {
        app->touchInputs[i].fingerDown   = 0;
        app->touchInputs[i].position     = {app->window.width / 2, app->window.height / 2};
        app->touchInputs[i].lastPosition = app->touchInputs[i].position;
    }

    SDL_DisplayMode display_mode;

    if (SDL_GetDesktopDisplayMode(0, &display_mode) == 0)
    {
#ifdef MOBILE
        app->window.width  = display_mode.w;
        app->window.height = display_mode.h;
#else
        app->window.width  = RESOLUTION_WIDTH;
        app->window.height = RESOLUTION_HEIGHT;
#endif
    }
    else
    {
        PRINT("SDL_GetCurrentDisplayMode error");
        return 5;
    }

    app->window.resolution.x = app->window.width;
    app->window.resolution.y = app->window.height;

    app->window.viewportScale = 1.0f;
    App_calculateWindowViewport(app->window.viewport, &app->window);

#ifdef MOBILE
    uint flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
    app->window.sdlWindow = SDL_CreateWindow(window_name,
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        app->window.width, app->window.height, flags);
#else
    uint flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL |SDL_WINDOW_RESIZABLE;
    app->window.sdlWindow = SDL_CreateWindow(window_name,
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        app->window.width, app->window.height, flags);
#endif

    if (!app->window.sdlWindow)
    {
        PRINT("SDL_CreateWindow() error: %s\n", SDL_GetError());
        return 6;
    }

    // Initialize OpenGL
    app->glContext = SDL_GL_CreateContext(app->window.sdlWindow);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, OPENGL_MAJORVERSION);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, OPENGL_MINORVERSION);
    SDL_GL_SetSwapInterval(VSYNC);
    glEnable(GL_SCISSOR_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#ifndef MOBILE
    glewInit();
#endif

    Draw_init();

    App_setFps(app, FPS_LIMIT);

    App_calculateWindowViewport(app->window.viewport, &app->window);

    app->window.color_r = 0.035f;
    app->window.color_g = 0.212f;
    app->window.color_b = 0.612f;
    app->window.color_a = 1.0f;

    return 0;
};

void
App_input(App *app)
{
    KEYSTATE = SDL_GetKeyboardState(NULL);

    for (int i = 0; i < NUM_TOUCH_FINGERS; ++i)
    {
        app->touchInputs[i].downThisFrame  = 0;
        app->touchInputs[i].upThisFrame    = 0;
    }

    SDL_Event event;

    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_QUIT:
            {
                App_shutDown(app);
            }break;
#ifdef MOBILE
            case SDL_FINGERMOTION:
            {
                app->fingerIndex = event.tfinger.fingerId;

                if (app->fingerIndex < NUM_TOUCH_FINGERS)
                {
                    app->touchInputs[app->fingerIndex].lastPosition = app->touchInputs[app->fingerIndex].position;
                    App_translateScreenCoordinatesByWindowViewport(
                            &app->touchInputs[app->fingerIndex].position.x,
                            &app->touchInputs[app->fingerIndex].position.y,
                            (int)(event.tfinger.x * (float)app->window.width),
                            (int)(event.tfinger.y * (float)app->window.height),
                            &app->window);
                }
            }break;

            case SDL_FINGERDOWN:
            {
                app->fingerIndex = event.tfinger.fingerId;

                if (app->fingerIndex < NUM_TOUCH_FINGERS)
                {
                    app->trueInput.x = event.button.x * app->window.viewportScale + app->window.viewport[0];
                    app->trueInput.y = event.button.y * app->window.viewportScale + app->window.viewport[1];

                    app->touchInputs[app->fingerIndex].fingerDown = 1;
                    app->touchInputs[app->fingerIndex].downThisFrame = 1;
                    app->touchInputs[app->fingerIndex].lastPosition = app->touchInputs[app->fingerIndex].position;
                    App_translateScreenCoordinatesByWindowViewport(
                            &app->touchInputs[app->fingerIndex].position.x,
                            &app->touchInputs[app->fingerIndex].position.y,
                            (int)(event.tfinger.x * (float)app->window.width),
                            (int)(event.tfinger.y * (float)app->window.height),
                            &app->window);

                    app->FingerDown();
                }
            }break;

            case SDL_FINGERUP:
            {
                app->fingerIndex = event.tfinger.fingerId;

                if (app->fingerIndex < NUM_TOUCH_FINGERS)
                {
                    app->trueInput.x = event.button.x * app->window.viewportScale + app->window.viewport[0];
                    app->trueInput.y = event.button.y * app->window.viewportScale + app->window.viewport[1];

                    app->touchInputs[app->fingerIndex].fingerDown = 0;
                    app->touchInputs[app->fingerIndex].upThisFrame = 1;
                    app->touchInputs[app->fingerIndex].lastPosition = app->touchInputs[app->fingerIndex].position;
                    App_translateScreenCoordinatesByWindowViewport(&app->touchInputs[app->fingerIndex].position.x,
                         &app->touchInputs[app->fingerIndex].position.y,
                         (int)(event.tfinger.x * (float)app->window.width),
                         (int)(event.tfinger.y * (float)app->window.height),
                         &app->window);
                    app->FingerUp();
                }
            }break;
#else
            case SDL_MOUSEMOTION:
            {
                switch (event.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                        CalculateMousePosition(app, event.button);
                        app->FingerMotion();
                    }break;
                }
            }break;

            case SDL_MOUSEBUTTONDOWN:
            {
                switch (event.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                        CalculateMousePosition(app, event.button);
                        app->FingerDown();
                    }break;
                }
            }break;

            case SDL_MOUSEBUTTONUP:
            {
                switch (event.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                        CalculateMousePosition(app, event.button);
                        app->FingerUp();
                    }break;
                }
            }break;

            case SDL_KEYDOWN:
            {
                app->keyboardInput = event.key.keysym.sym;
                app->KeyDown();
            }break;

            case SDL_KEYUP:
            {
                app->keyboardInput = event.key.keysym.sym;
                app->KeyUp();
            }break;

            case SDL_WINDOWEVENT:
            {
                switch (event.window.event)
                {
                    case SDL_WINDOWEVENT_CLOSE: App_shutDown(app); break;
                    case SDL_WINDOWEVENT_RESIZED:
                    {
                        app->window.width  = event.window.data1;
                        app->window.height = event.window.data2;

                        App_calculateWindowViewport(app->window.viewport, &app->window);
                    }break;
                }
            }break;
#endif
        }
    }
}

void
App_update(App *app)
{
    app->Update();
}

void
App_render(App *app)
{
    glViewport(0, 0, app->window.width, app->window.height);
    glClearColor(app->window.color_r, app->window.color_g, app->window.color_b, app->window.color_a);
    glClear(GL_COLOR_BUFFER_BIT);

    app->window.viewport[2] =
        app->window.viewport[0] + app->window.resolution.x * app->window.viewportScale;
    app->window.viewport[3] =
        app->window.viewport[1] + app->window.resolution.y * app->window.viewportScale;

    auto projection = Draw_prepRender(app->window.viewport);

    if (app->PreRender)
    {
        app->PreRender();
        Draw_flush(app->window.viewport, projection.view);
    }

    if (app->Render)
    {
        app->Render();
        Draw_flush(app->window.viewport, projection.view);
    }
    
    if (app->PostRender)
    {
        app->Render();
        Draw_flush(app->window.viewport, projection.view);
    }

    SDL_GL_SwapWindow(app->window.sdlWindow);
}

void
App_shutDown(App* app)
{
    app->running = 0;
}

void
App_calculateWindowViewport(float *viewport, Window *window)
{
    viewport[0] = 0;
    viewport[1] = 0;
    viewport[2] = (float)window->resolution.x;
    viewport[3] = (float)window->resolution.y;
}

inline void
App_translateScreenCoordinatesByWindowViewport(uint *x, uint *y,
    int real_x, int real_y, Window *window)
{
    *x = (uint)(real_x * window->viewportScale + window->viewport[0]);
    *y = (uint)(real_y * window->viewportScale + window->viewport[1]);
}

void
App_setFps(App *app, int amount)
{
    app->clock.targetFps = amount;
}

double
App_getTimeSinceAppStarted(App *app)
{
    return (uint)(time(0) - app->appStartTime);
}

void
Clock_tick(Clock *clock)
{
    double tick_time = (double)SDL_GetPerformanceCounter();
    double perf_freq = (double)SDL_GetPerformanceFrequency();
    clock->delta = (tick_time - clock->lastTick) / perf_freq;
    clock->lastTick = tick_time;
    clock->fps = 1.0f / clock->delta;

    if (clock->targetFps != 0)
    {
        double target_time = 1.0f / (double)clock->targetFps * 1000;

        if (tick_time - clock->lastTick < target_time)
        {
            uint32_t substraction = (uint32_t)tick_time - (uint32_t)clock->lastTick;
            usleep(((uint32_t)target_time - substraction) * 1000);
        }
    }
}

void
CalculateMousePosition(App *app, SDL_MouseButtonEvent button)
{
    SDL_GetMouseState(&button.x, &button.y);

    app->touchInputs[MB_LEFT].lastPosition = app->touchInputs[MB_LEFT].position;
    app->touchInputs[MB_LEFT].position.x = (uint)(button.x * app->window.viewportScale + app->window.viewport[0]);
    app->touchInputs[MB_LEFT].position.y = (uint)(button.y * app->window.viewportScale + app->window.viewport[1]);

    app->trueInput.x = (int)(button.x * app->window.viewportScale + app->window.viewport[0]);
    app->trueInput.y = (int)(button.y * app->window.viewportScale + app->window.viewport[1]);
}

void
App_setWindowColor(Window *window, float r, float g, float b, float a)
{
    window->color_r = r;
    window->color_g = g;
    window->color_b = b;
    window->color_a = a;
}

#ifndef MOBILE
void usleep(DWORD waitTime){
	LARGE_INTEGER perfCnt, start, now;

	QueryPerformanceFrequency(&perfCnt);
	QueryPerformanceCounter(&start);

	do {
		QueryPerformanceCounter((LARGE_INTEGER*) &now);
	} while ((now.QuadPart - start.QuadPart) / float(perfCnt.QuadPart) * 1000 * 1000 < waitTime);
}
#endif
