#pragma once

#include "defs.h"
#include "utils.h"

#ifdef MOBILE
#include "deps/SDL2_mixer-2.0.2/SDL_mixer.h"
#else
#include "deps/SDL2/SDL_mixer.h"
#endif

typedef Mix_Chunk SoundClip;
typedef Mix_Music MusicClip;

SoundClip*
Audio_loadSound(char *path);

void
Audio_setVolume(int channel, int value);

void
Audio_setAllVolume(int value);

void
Audio_playSound(SoundClip *sound);

void
Audio_playSound(SoundClip *sound, int loops);

void
Audio_playSound(SoundClip *sound, int channel, int loops);

void
Audio_playMusic(SoundClip *music, int loops);
