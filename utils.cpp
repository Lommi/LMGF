#include "utils.h"

bool
Utils_writeFile(char *org, char *gameName, char *fileName, void *data, int len)
{
#ifdef MOBILE
    char *basePath = SDL_GetPrefPath(org, gameName);
    char *writePosition = 0;

    if (basePath)
    {
        char buffer[2048];
        strncpy(buffer, basePath, 2048);
        uint size = strlen(buffer);
        writePosition = &buffer[size];
        strcpy(writePosition, filename);
        PRINT("App_writeFile : Filepath found: %s\n", buffer);
    }
    SDL_RWops *rw = SDL_RWFromFile(writePosition, "w + b");
    if (rw != NULL)
    {
        if (SDL_RWwrite(rw, data, len, len) != len)
        {
            PRINT("Couldn't fully write data to file %s! %s\n", fileName, SDL_GetError());
            return false;
        }

        PRINT("Succesfully write to file: %s\n", fileName);
        SDL_RWclose(rw);
        return true;
    }
    else
    {
        SDL_Log("Could not open file %s. %s", fileName, SDL_GetError());
        return false;
    }
#else
    FILE *file = 0;
    file = fopen(fileName, "w + b");
    if (!file)
    {
        PRINT("Failed to write into %s\n", fileName);
        return false;
    }
    fwrite(data, 1, len, file);
    fclose(file);
    return true;
#endif
}

bool
Utils_readFile(char *org, char *gameName, char *fileName, void *output, int len)
{
#ifdef MOBILE
    char *basePath = SDL_GetPrefPath(org, gameName);
    char *readPosition = 0;

    if (basePath)
    {
        char buffer[2048];
        strncpy(buffer, basePath, 2048);
        uint size = strlen(buffer);
        readPosition = &buffer[size];
        strcpy(readPosition, fileName);
        PRINT("App_readFile : Filepath found : %s\n", buffer);
    }

    SDL_RWops *rw = SDL_RWFromFile(readPosition, "r + b");
    if (rw != NULL)
    {
        if (SDL_RWread(rw, output, 1, len) != len)
        {
            PRINT("Couldn't fully read data from file %s! %s\n", fileName, SDL_GetError());
            return false;
        }

        PRINT("Succesfully read from file: %s\n", fileName);
        SDL_RWclose(rw);
        return true;
    }
    else
    {
        PRINT("Could not open file %s. %s", fileName, SDL_GetError());
        return false;
    }
#else
    FILE *file = 0;
    file = fopen(fileName, "r + b");
    if (!file)
    {
        PRINT("Failed to read from %s\n", fileName);
        return false;
    }
    fread(output, 1, len, file);
    fclose(file);

    return true;
#endif
}


#ifndef MOBILE
char *
Utils_readFile(char *fileName)
{
    FILE *fp;
    char *output;

    fp = fopen(fileName, "r+");

    if (fp == NULL)
    {
        PRINT("Could not open file %s", fileName);
        fclose(fp);
        return NULL;
    }

    int c, cval, numChars = 0;
    size_t nread = 0;

    output = (char*)malloc(sizeof(char));

    while ((c = fgetc(fp)) != EOF)
    {
        ++numChars;
    }

    rewind(fp);
    output = (char*)realloc(output, sizeof(char) * (numChars + 1));

    while ((c = fgetc(fp)) != EOF)
    {
        cval = nread + 1;
        output[nread++] = (char)c;
    }
    output[nread] = '\0';
    fclose(fp);

    return output;
}
#endif
