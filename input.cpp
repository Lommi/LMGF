#include "input.h"

#ifdef MOBILE
inline void
Input_calculateTouchPosition(Input *input, SDL_Event *event, Window *window)
{
    input->trueInput.x = event->button.x * window->viewportScale + window->viewport[0];
    input->trueInput.y = event->button.y * window->viewportScale + window->viewport[1];

    input->touchInputs[input->fingerIndex].lastPosition = input->touchInputs[input->fingerIndex].position;
    Window_translateScreenCoordinatesByViewport(
            &input->touchInputs[input->fingerIndex].position.x,
            &input->touchInputs[input->fingerIndex].position.y,
            (int)(event->tfinger.x * (float)window->width),
            (int)(event->tfinger.y * (float)window->height),
            &window);
}
#else
inline void
Input_calculateMousePosition(Input *input, Window *window, SDL_MouseButtonEvent button)
{
    SDL_GetMouseState(&button.x, &button.y);

    input->touchInputs[MOUSE_LEFT].lastPosition = input->touchInputs[MOUSE_LEFT].position;
    input->touchInputs[MOUSE_LEFT].position.x = (uint)(button.x * window->viewportScale + window->viewport[0]);
    input->touchInputs[MOUSE_LEFT].position.y = (uint)(button.y * window->viewportScale + window->viewport[1]);

    input->trueInput.x = (int)(button.x * window->viewportScale + window->viewport[0]);
    input->trueInput.y = (int)(button.y * window->viewportScale + window->viewport[1]);
}
#endif

void
Input_update(Input *input, Window *window)
{
    KEYSTATE = SDL_GetKeyboardState(NULL);

    for (int i = 0; i < NUM_TOUCH_FINGERS; ++i)
    {
        input->touchInputs[i].downThisFrame  = 0;
        input->touchInputs[i].upThisFrame    = 0;
    }

    SDL_Event event;

    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
#ifdef MOBILE
            case SDL_FINGERMOTION:
            {
                input->fingerIndex = event.tfinger.fingerId;

                if (input->fingerIndex < NUM_TOUCH_FINGERS)
                {
                    Input_calculateTouchPosition(input, &event, window);

                    if (input->FingerMotion)
                    {
                        input->FingerMotion();
                    }
                }
            }break;

            case SDL_FINGERDOWN:
            {
                input->fingerIndex = event.tfinger.fingerId;

                if (input->fingerIndex < NUM_TOUCH_FINGERS)
                {
                    input->touchInputs[input->fingerIndex].fingerDown = 1;
                    input->touchInputs[input->fingerIndex].downThisFrame = 1;
                    Input_calculateTouchPosition(input, &event, window);

                    if (input->FingerDown)
                    {
                        input->FingerDown();
                    }
                }
            }break;

            case SDL_FINGERUP:
            {
                input->fingerIndex = event.tfinger.fingerId;

                if (input->fingerIndex < NUM_TOUCH_FINGERS)
                {
                    input->touchInputs[input->fingerIndex].fingerDown = 0;
                    input->touchInputs[input->fingerIndex].upThisFrame = 1;
                    Input_calculateTouchPosition(input, &event, window);

                    if (input->FingerUp)
                    {
                        input->FingerUp();
                    }
                }
            }break;
#else
            case SDL_MOUSEMOTION:
            {
                switch (event.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                        Input_calculateMousePosition(input, window, event.button);

                        if (input->FingerMotion)
                        {
                            input->FingerMotion();
                        }
                    }break;
                }
            }break;

            case SDL_MOUSEBUTTONDOWN:
            {
                switch (event.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                        input->touchInputs[input->fingerIndex].fingerDown = 1;
                        input->touchInputs[input->fingerIndex].downThisFrame = 1;
                        Input_calculateMousePosition(input, window, event.button);

                        if (input->FingerDown)
                        {
                            input->FingerDown();
                        }
                    }break;
                }
            }break;

            case SDL_MOUSEBUTTONUP:
            {
                switch (event.button.button)
                {
                    case SDL_BUTTON_LEFT:
                    {
                        input->touchInputs[input->fingerIndex].fingerDown = 0;
                        input->touchInputs[input->fingerIndex].upThisFrame = 1;
                        Input_calculateMousePosition(input, window, event.button);

                        if (input->FingerUp)
                        {
                            input->FingerUp();
                        }
                    }break;
                }
            }break;

            case SDL_KEYDOWN:
            {
                input->keyboardInput = event.key.keysym.sym;

                if (input->KeyDown)
                {
                    input->KeyDown();
                }
            }break;

            case SDL_KEYUP:
            {
                input->keyboardInput = event.key.keysym.sym;

                if (input->KeyUp)
                {
                    input->KeyUp();
                }
            }break;

            case SDL_WINDOWEVENT:
            {
                switch (event.window.event)
                {
                    case SDL_WINDOWEVENT_RESIZED:
                    {
                        Window_setResolution(window, event.window.data1, event.window.data2);
                        Window_setViewport(window, 0, 0, window->width, window->height);
                    }break;
                }
            }break;
#endif
        }
    }
}
