#pragma once

#include <errno.h>
#include <time.h>

#include "defs.h"

#ifdef MOBILE
#include <unistd.h>
#include "deps/SDL2-2.0.8/include/SDL.h"
#include "deps/SDL2-2.0.8/include/SDL_opengles.h"
#include "deps/SDL2_mixer-2.0.2/SDL_mixer.h"
#include "deps/SDL2_image-2.0.2/SDL_image.h"
#include "deps/SDL2_ttf-2.0.14/SDL_ttf.h"
#else
#include <stdio.h>
#include <Windows.h>
#include "deps/SDL2/SDL.h"
#include "deps/SDL2/SDL_image.h"
#include "deps/SDL2/SDL_ttf.h"
#include "deps/SDL2/SDL_mixer.h"
#include "deps/GL/glew.h"
#include "deps/SDL2/SDL_opengl.h"
#endif

#include "window.h"
#include "utils.h"
#include "input.h"
#include "draw.h"
#include "text.h"
#include "audio.h"

typedef struct Clock            Clock;
typedef struct App              App;

static SDL_Color colorWhite       = {0xFF, 0xFF, 0xFF, 0xFF};
static SDL_Color colorBlack       = {0x00, 0x00, 0x00, 0xFF};
static SDL_Color colorRed         = {0x00, 0x00, 0xFF, 0xFF};
static SDL_Color colorGreen       = {0x00, 0xFF, 0x00, 0xFF};
static SDL_Color colorBlue        = {0xFF, 0x00, 0x00, 0xFF};
static SDL_Color colorYellow      = {0x00, 0xFF, 0xFF, 0xFF};
static SDL_Color colorPurple      = {0xFF, 0x00, 0xFF, 0xFF};
static SDL_Color colorOrange      = {0x00, 0xA5, 0xFF, 0xFF};
static SDL_Color colorAqua        = {0xFF, 0xFF, 0x00, 0xFF};

#ifndef MOBILE
void usleep(DWORD waitTime);
#endif

void
App_mainloop(App *app, Input *input, Renderer *renderer);

int
App_init(App *app, const char *windowName, const char *lmgfFolderPath, const char *assetsFolderPath); // Return 0 on success

void
App_update(App *app);

void
App_shutDown(App *app);

void
App_setFps(App *app, int amount);

double
App_getTimeSinceAppStarted(App *app);

void
Clock_tick(Clock *clock);

struct Clock
{
    double delta;      // Format: seconds
    double lastTick;
    double fps;
    bool32 limitFps;
    uint   targetFps;
};

struct App
{
    time_t                      appStartTime;
    volatile bool32	            running;
    Clock				        clock;
    SDL_GLContext		        glContext;
    Window                      window;

    void                        (*Update)();
};
