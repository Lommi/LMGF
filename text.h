#pragma once

#include "draw.h"
#include "defs.h"

#ifdef MOBILE
#include "deps/SDL2_ttf-2.0.14/SDL_ttf.h"
#else
#include "deps/SDL2/SDL_ttf.h"
#endif

typedef TTF_Font Font;

Font*
Text_loadFont(char *path, int size);

int
Text_create(Texture *texture, char *text,
    Font *font, SDL_Color color, unsigned int wrapWidth);
