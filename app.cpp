#include "app.h"

void
App_mainloop(App *app, Input *input, Renderer *renderer)
{
    app->running = 1;

    while (app->running)
    {
#ifndef MOBILE
        Clock_tick(&app->clock);
#endif
        Input_update(input, &app->window);
        App_update(app);
        Draw_render(renderer, &app->window);
    }
}

int
App_init(App *app, const char *windowName, const char *lmgfFolderPath, const char *assetsFolderPath)
{
    Defs_initFolderPaths(lmgfFolderPath, assetsFolderPath);

    app->appStartTime = time(0);

    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        PRINT("SDL_Init() error: %s\n", SDL_GetError());
        return 1;
    }

    if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 1024) < 0)
    {
        PRINT("Failed to open audio. Mix_OpenAudio() error: %s\n", Mix_GetError());
        return 2;
    }

    Mix_AllocateChannels(NUM_AUDIO_CHANNELS);
    for (int i = 0; i < NUM_AUDIO_CHANNELS; ++i)
    {
        Mix_Volume(i, MAX_VOLUME);
    }

    if (TTF_Init() == -1)
    {
        PRINT("TTF_Init() error: %s\n", TTF_GetError());
        return 3;
    }

    if (IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG) < 0)
    {
        PRINT("IMG_Init error: %s\n", IMG_GetError());
        return 4;
    }

    srand((uint)time(0));

    SDL_DisplayMode displayMode;

    if (SDL_GetDesktopDisplayMode(0, &displayMode) == 0)
    {
#ifdef MOBILE
        Window_setResolution(&app->window, displayMode.w, displayMode.h);
#else
        Window_setResolution(&app->window, RESOLUTION_WIDTH, RESOLUTION_HEIGHT);
#endif
    }
    else
    {
        PRINT("SDL_GetCurrentDisplayMode error");
        return 5;
    }

    app->window.viewportScale = 1.0f;
    Window_setViewport(&app->window, 0, 0, app->window.width, app->window.height);

#ifdef MOBILE
    uint flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
    app->window.sdlWindow = SDL_CreateWindow(windowName,
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        app->window.width, app->window.height, flags);
#else
    uint flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL |SDL_WINDOW_RESIZABLE;
    app->window.sdlWindow = SDL_CreateWindow(windowName,
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        app->window.width, app->window.height, flags);
#endif

    if (!app->window.sdlWindow)
    {
        PRINT("SDL_CreateWindow() error: %s\n", SDL_GetError());
        return 6;
    }

    // Initialize OpenGL
    app->glContext = SDL_GL_CreateContext(app->window.sdlWindow);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, OPENGL_MAJORVERSION);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, OPENGL_MINORVERSION);
    SDL_GL_SetSwapInterval(VSYNC);
    glEnable(GL_SCISSOR_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#ifndef MOBILE
    glewInit();
#endif

    Draw_init();

    App_setFps(app, FPS_LIMIT);

    Window_setViewport(&app->window, 0, 0, app->window.width, app->window.height);
    Window_setColor(&app->window, 0.035f, 0.212f, 0.612f, 1.0f);

    return 0;
};

void
App_update(App *app)
{
    app->Update();
}

void
App_shutDown(App* app)
{
    app->running = 0;
}

void
App_setFps(App *app, int amount)
{
    app->clock.targetFps = amount;
}

double
App_getTimeSinceAppStarted(App *app)
{
    return (uint)(time(0) - app->appStartTime);
}

void
Clock_tick(Clock *clock)
{
    double tickTime = (double)SDL_GetPerformanceCounter();
    double perfFreq = (double)SDL_GetPerformanceFrequency();
    clock->delta = (tickTime - clock->lastTick) / perfFreq;
    clock->lastTick = tickTime;
    clock->fps = 1.0f / clock->delta;

    if (clock->targetFps != 0)
    {
        double targetTime = 1.0f / (double)clock->targetFps * 1000;

        if (tickTime - clock->lastTick < targetTime)
        {
            uint32_t substraction = (uint32_t)tickTime - (uint32_t)clock->lastTick;
            usleep(((uint32_t)targetTime - substraction) * 1000);
        }
    }
}

#ifndef MOBILE
void usleep(DWORD waitTime){
	LARGE_INTEGER perfCnt, start, now;

	QueryPerformanceFrequency(&perfCnt);
	QueryPerformanceCounter(&start);

	do {
		QueryPerformanceCounter((LARGE_INTEGER*) &now);
	} while ((now.QuadPart - start.QuadPart) / float(perfCnt.QuadPart) * 1000 * 1000 < waitTime);
}
#endif
