#pragma once
#include <cstdint>
#include <time.h>
#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <math.h>
#include <vector>
#include <list>
#include <string.h>

typedef int             bool32;
typedef unsigned int    uint;
typedef struct vec2     vec2;
typedef struct fvec2    fvec2;
typedef struct uvec2    uvec2;
typedef struct rect     rect;
typedef struct frect    frect;

//#define MOBILE

#define VSYNC 1

#define OPENGL_MAJORVERSION 2
#define OPENGL_MINORVERSION 0

#define FPS_LIMIT 60

#define RESOLUTION_WIDTH 1280
#define RESOLUTION_HEIGHT 800

#define NUM_TOUCH_FINGERS 10

#define MAX_VOLUME 50
#define NUM_AUDIO_CHANNELS 3

#define PI 3.1415926535897
#define RADIAN PI / 180

#define BATCH_SIZE 1024
#define FLOATS_PER_SPRITE 16
#define FLOATS_PER_TRIANGLE 9

#define MIN(val, min)\
    (val >= min ? val : min)

#define MAX(val, max)\
    (val <= max ? val : max)

#define CLAMP(val, min, max)\
    (MIN(MAX(val, max), min))

extern char *LMGF_PATH;
extern char *ASSETS_PATH;

static char SHADER_SPRITE_FRAG[] = "#version 100\n"
"precision highp float;\n"
"varying vec2 texCoord;\n"
"uniform sampler2D tex;\n"
"void main(void)\n"
"{\n"
"    gl_FragColor = texture2D(tex, texCoord);\n"
"}";

static char SHADER_SPRITE_VERT[] = "#version 100\n"
"precision highp float;\n"
"attribute vec2 pos;\n"
"attribute vec2 inTexCoord;\n"
"varying vec2 texCoord;\n"
"uniform mat4 p;\n"
"void main(void) \n"
"{\n"
"    gl_Position = p * vec4(pos.x, pos.y, 0.0, 1.0);\n"
"    texCoord = inTexCoord;\n"
"}";

static char SHADER_GEOMETRY_FRAG[] = "#version 100\n"
"precision highp float;\n"
"varying vec4 color;\n"
"void main(void)\n"
"{\n"
"    gl_FragColor = color;\n"
"}";

static char SHADER_GEOMETRY_VERT[] = "#version 100\n"
"precision lowp float;\n"
"attribute vec2 pos;\n"
"attribute vec4 inColor;\n"
"varying vec4 color;\n"
"uniform mat4 p;\n"
"void main(void)\n"
"{\n"
"    gl_Position = p * vec4(pos.x, pos.y, 0.0, 1.0);\n"
"    color = inColor;\n"
"}";

void
Defs_initFolderPaths(const char *lmgfFolderPath, const char *assetsFolderPath);

struct uvec2
{
    uint x, y;
};

struct vec2
{
    int x, y;
};

struct fvec2
{
    float x, y;
};

struct rect
{
    int x, y, w, h;
};

struct frect
{
    float x, y, w, h;
};

#ifdef MOBILE
    #include <SDL.h>
    #define PRINT(str, ...)\
        SDL_Log(str, ##__VA_ARGS__)
    #define SET_BREAKPOINT()\
        raise(SIGINT)
#else
    #define PRINT(str, ...)\
        printf(str, ##__VA_ARGS__)
    #define SET_BREAKPOINT()
#endif

/* Triangle macro corner ordering:
* 1. left
* 2. right
* 3. top */

#define SET_TRIANGLE()\
    drawBatch.triangleVertexBuffer[index + 0] = (GLfloat)x1;\
    drawBatch.triangleVertexBuffer[index + 1] = (GLfloat)y1;\
    drawBatch.triangleVertexBuffer[index + 3] = (GLfloat)x2;\
    drawBatch.triangleVertexBuffer[index + 4] = (GLfloat)y2;\
    drawBatch.triangleVertexBuffer[index + 6] = (GLfloat)x3;\
    drawBatch.triangleVertexBuffer[index + 7] = (GLfloat)y3;\
    GLubyte *base = (GLubyte*)&drawBatch.triangleVertexBuffer[index];\
    GLubyte *firstColor  = base + 8;\
    GLubyte *secondColor = base + 20;\
    GLubyte *thirdColor  = base + 32;\
    GLubyte *alphaValue  = base + 44;\
    firstColor[0] = (GLubyte)r;\
    firstColor[1] = (GLubyte)g;\
    firstColor[2] = (GLubyte)b;\
    firstColor[3] = (GLubyte)a;\
    secondColor[0] = (GLubyte)r;\
    secondColor[1] = (GLubyte)g;\
    secondColor[2] = (GLubyte)b;\
    secondColor[3] = (GLubyte)a;\
    thirdColor[0] = (GLubyte)r;\
    thirdColor[1] = (GLubyte)g;\
    thirdColor[2] = (GLubyte)b;\
    thirdColor[3] = (GLubyte)a;\
    alphaValue[0] = (GLubyte)r;\
    alphaValue[1] = (GLubyte)g;\
    alphaValue[2] = (GLubyte)b;\
    alphaValue[3] = (GLubyte)a;

#define SET_TRIANGLE_GRADIENT()\
    drawBatch.triangleVertexBuffer[index + 0] = (GLfloat)x1;\
    drawBatch.triangleVertexBuffer[index + 1] = (GLfloat)y1;\
    drawBatch.triangleVertexBuffer[index + 3] = (GLfloat)x2;\
    drawBatch.triangleVertexBuffer[index + 4] = (GLfloat)y2;\
    drawBatch.triangleVertexBuffer[index + 6] = (GLfloat)x3;\
    drawBatch.triangleVertexBuffer[index + 7] = (GLfloat)y3;\
    GLubyte *base = (GLubyte*)&drawBatch.triangleVertexBuffer[index];\
    GLubyte *firstColor  = base + 8;\
    GLubyte *secondColor = base + 20;\
    GLubyte *thirdColor  = base + 32;\
    GLubyte *alphaValue  = base + 44;\
    firstColor[0] = (GLubyte)c1.r;\
    firstColor[1] = (GLubyte)c1.g;\
    firstColor[2] = (GLubyte)c1.b;\
    firstColor[3] = (GLubyte)c1.a;\
    secondColor[0] = (GLubyte)c2.r;\
    secondColor[1] = (GLubyte)c2.g;\
    secondColor[2] = (GLubyte)c2.b;\
    secondColor[3] = (GLubyte)c2.a;\
    thirdColor[0] = (GLubyte)c3.r;\
    thirdColor[1] = (GLubyte)c3.g;\
    thirdColor[2] = (GLubyte)c3.b;\
    thirdColor[3] = (GLubyte)c3.a;\
    alphaValue[0] = (GLubyte)ca.r;\
    alphaValue[1] = (GLubyte)ca.g;\
    alphaValue[2] = (GLubyte)ca.b;\
    alphaValue[3] = (GLubyte)ca.a;

/* Rectangle macro corner ordering:
* 1. top left
* 2. top right
* 3. bottom left
* 4. bottom right */

#define SPRITE_DRAW_FUNC_LOWER_HALF()\
    if (drawBatch.spriteCount > 0)\
    {\
        if (drawBatch.textureBuffer[drawBatch.texSwapCount].id == texture->id)\
            ++drawBatch.textureBuffer[drawBatch.texSwapCount].repeats;\
        else\
        {\
            ++drawBatch.texSwapCount;\
            drawBatch.textureBuffer[drawBatch.texSwapCount].id = texture->id;\
            drawBatch.textureBuffer[drawBatch.texSwapCount].repeats = 0;\
        }\
    }\
    else\
    {\
        drawBatch.textureBuffer[0].id = texture->id;\
        drawBatch.textureBuffer[0].repeats = 0;\
    }\
    ++drawBatch.spriteCount;

#define SET_CLIPPED_SPRITE_DEFAULT_POSITION()\
    drawBatch.spriteVertexBuffer[index +  0] = (GLfloat)x;\
    drawBatch.spriteVertexBuffer[index +  1] = (GLfloat)y;\
    drawBatch.spriteVertexBuffer[index +  4] = (GLfloat)(x + clip[2]);\
    drawBatch.spriteVertexBuffer[index +  5] = (GLfloat)y;\
    drawBatch.spriteVertexBuffer[index +  8] = (GLfloat)x;\
    drawBatch.spriteVertexBuffer[index +  9] = (GLfloat)(y + clip[3]);\
    drawBatch.spriteVertexBuffer[index + 12] = (GLfloat)(x + clip[2]);\
    drawBatch.spriteVertexBuffer[index + 13] = (GLfloat)(y + clip[3]);

#define SET_UNCLIPPED_SPRITE_DEFAULT_POSITION()\
    drawBatch.spriteVertexBuffer[index +  0] = (GLfloat)x;\
    drawBatch.spriteVertexBuffer[index +  1] = (GLfloat)y;\
    drawBatch.spriteVertexBuffer[index +  4] = (GLfloat)(x + texture->width);\
    drawBatch.spriteVertexBuffer[index +  5] = (GLfloat)y;\
    drawBatch.spriteVertexBuffer[index +  8] = (GLfloat)x;\
    drawBatch.spriteVertexBuffer[index +  9] = (GLfloat)(y + texture->height);\
    drawBatch.spriteVertexBuffer[index + 12] = (GLfloat)(x + texture->width);\
    drawBatch.spriteVertexBuffer[index + 13] = (GLfloat)(y + texture->height);

#define SET_UNCLIPPED_SPRITE_SCALED_POSITION()\
    drawBatch.spriteVertexBuffer[index +  0] = (GLfloat)x;\
    drawBatch.spriteVertexBuffer[index +  1] = (GLfloat)y;\
    drawBatch.spriteVertexBuffer[index +  4] = (GLfloat)(x + texture->width * scaleX);\
    drawBatch.spriteVertexBuffer[index +  5] = (GLfloat)y;\
    drawBatch.spriteVertexBuffer[index +  8] = (GLfloat)x;\
    drawBatch.spriteVertexBuffer[index +  9] = (GLfloat)(y + texture->height * scaleY);\
    drawBatch.spriteVertexBuffer[index + 12] = (GLfloat)(x + texture->width * scaleX);\
    drawBatch.spriteVertexBuffer[index + 13] = (GLfloat)(y + texture->height * scaleY);

#define SET_CLIPPED_SPRITE_SCALED_POSITION()\
    drawBatch.spriteVertexBuffer[index +  0] = (GLfloat)x;\
    drawBatch.spriteVertexBuffer[index +  1] = (GLfloat)y;\
    drawBatch.spriteVertexBuffer[index +  4] = (GLfloat)(x + clip[2] * scaleX);\
    drawBatch.spriteVertexBuffer[index +  5] = (GLfloat)y;\
    drawBatch.spriteVertexBuffer[index +  8] = (GLfloat)x;\
    drawBatch.spriteVertexBuffer[index +  9] = (GLfloat)(y + clip[3] * scaleY);\
    drawBatch.spriteVertexBuffer[index + 12] = (GLfloat)(x + clip[2] * scaleX);\
    drawBatch.spriteVertexBuffer[index + 13] = (GLfloat)(y + clip[3] * scaleY);

#define SET_CLIPPED_SPRITE_DEFAULT_CLIPS()\
    drawBatch.spriteVertexBuffer[index +  2] = (GLfloat)clip[0] / (GLfloat)texture->width;\
    drawBatch.spriteVertexBuffer[index +  3] = (GLfloat)clip[1] / (GLfloat)texture->height;\
    drawBatch.spriteVertexBuffer[index +  6] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
    drawBatch.spriteVertexBuffer[index +  7] = (GLfloat)clip[1] / (GLfloat)texture->height;\
    drawBatch.spriteVertexBuffer[index + 10] = (GLfloat)clip[0] / (GLfloat)texture->width;\
    drawBatch.spriteVertexBuffer[index + 11] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
    drawBatch.spriteVertexBuffer[index + 14] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
    drawBatch.spriteVertexBuffer[index + 15] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\

#define SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS()\
    drawBatch.spriteVertexBuffer[index +  2] = 0.0f;\
    drawBatch.spriteVertexBuffer[index +  3] = 0.0f;\
    drawBatch.spriteVertexBuffer[index +  6] = 1.0f;\
    drawBatch.spriteVertexBuffer[index +  7] = 0.0f;\
    drawBatch.spriteVertexBuffer[index + 10] = 0.0f;\
    drawBatch.spriteVertexBuffer[index + 11] = 1.0f;\
    drawBatch.spriteVertexBuffer[index + 14] = 1.0f;\
    drawBatch.spriteVertexBuffer[index + 15] = 1.0f;\

#define SET_CLIPPED_SPRITE_FLIPPED_CLIPS()\
    switch (flip)\
    {\
        case SPRITE_FLIP_NONE:\
        {\
            drawBatch.spriteVertexBuffer[index +  2] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index +  3] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index +  6] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index +  7] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index + 10] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index + 11] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index + 14] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index + 15] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
        }\
            break;\
        case SPRITE_FLIP_HORIZONTAL:\
        {\
            drawBatch.spriteVertexBuffer[index +  2] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index +  3] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index +  6] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index +  7] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index + 10] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index + 11] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index + 14] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index + 15] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
        }\
            break;\
        case SPRITE_FLIP_VERTICAL:\
        {\
            drawBatch.spriteVertexBuffer[index +  2] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index +  3] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index +  6] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index +  7] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index + 10] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index + 11] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index + 14] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index + 15] = (GLfloat)clip[1] / (GLfloat)texture->height;\
        }\
            break;\
        case SPRITE_FLIP_BOTH:\
        {\
            drawBatch.spriteVertexBuffer[index +  2] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index +  3] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index +  6] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index +  7] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index + 10] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index + 11] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            drawBatch.spriteVertexBuffer[index + 14] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            drawBatch.spriteVertexBuffer[index + 15] = (GLfloat)clip[1] / (GLfloat)texture->height;\
        }\
            break;\
    }

#define SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS()\
        switch (flip)\
        {\
            case SPRITE_FLIP_NONE:\
            {\
                drawBatch.spriteVertexBuffer[index +  2] = 0.0f;\
                drawBatch.spriteVertexBuffer[index +  3] = 0.0f;\
                drawBatch.spriteVertexBuffer[index +  6] = 1.0f;\
                drawBatch.spriteVertexBuffer[index +  7] = 0.0f;\
                drawBatch.spriteVertexBuffer[index + 10] = 0.0f;\
                drawBatch.spriteVertexBuffer[index + 11] = 1.0f;\
                drawBatch.spriteVertexBuffer[index + 14] = 1.0f;\
                drawBatch.spriteVertexBuffer[index + 15] = 1.0f;\
            }\
                break;\
            case SPRITE_FLIP_HORIZONTAL:\
            {\
                drawBatch.spriteVertexBuffer[index +  2] = 1.0f;\
                drawBatch.spriteVertexBuffer[index +  3] = 0.0f;\
                drawBatch.spriteVertexBuffer[index +  6] = 0.0f;\
                drawBatch.spriteVertexBuffer[index +  7] = 0.0f;\
                drawBatch.spriteVertexBuffer[index + 10] = 1.0f;\
                drawBatch.spriteVertexBuffer[index + 11] = 1.0f;\
                drawBatch.spriteVertexBuffer[index + 14] = 0.0f;\
                drawBatch.spriteVertexBuffer[index + 15] = 1.0f;\
            }\
                break;\
            case SPRITE_FLIP_VERTICAL:\
            {\
                drawBatch.spriteVertexBuffer[index +  2] = 0.0f;\
                drawBatch.spriteVertexBuffer[index +  3] = 1.0f;\
                drawBatch.spriteVertexBuffer[index +  6] = 1.0f;\
                drawBatch.spriteVertexBuffer[index +  7] = 1.0f;\
                drawBatch.spriteVertexBuffer[index + 10] = 0.0f;\
                drawBatch.spriteVertexBuffer[index + 11] = 0.0f;\
                drawBatch.spriteVertexBuffer[index + 14] = 1.0f;\
                drawBatch.spriteVertexBuffer[index + 15] = 0.0f;\
            }\
                break;\
            case SPRITE_FLIP_BOTH:\
            {\
                drawBatch.spriteVertexBuffer[index +  2] = 1.0f;\
                drawBatch.spriteVertexBuffer[index +  3] = 1.0f;\
                drawBatch.spriteVertexBuffer[index +  6] = 0.0f;\
                drawBatch.spriteVertexBuffer[index +  7] = 1.0f;\
                drawBatch.spriteVertexBuffer[index + 10] = 1.0f;\
                drawBatch.spriteVertexBuffer[index + 11] = 0.0f;\
                drawBatch.spriteVertexBuffer[index + 14] = 0.0f;\
                drawBatch.spriteVertexBuffer[index + 15] = 0.0f;\
            }\
                break;\
        }

#define ROTATE_SPRITE()\
    float transX, transY, rotX, rotY;\
    \
    for (int i = 0; i < 4; ++i)\
    {\
        transX = drawBatch.spriteVertexBuffer[index + (i * 4) + 0] - originX;\
        transY = drawBatch.spriteVertexBuffer[index + (i * 4) + 1] - originY;\
        \
        rotX = transX * cos(angle) - transY * sin(angle);\
        rotY = transX * sin(angle) + transY * cos(angle);\
        \
        drawBatch.spriteVertexBuffer[index + (i * 4) + 0] = rotX + originX;\
        drawBatch.spriteVertexBuffer[index + (i * 4) + 1] = rotY + originY;\
    }
