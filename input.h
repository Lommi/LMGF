#pragma once

#include "defs.h"
#include "window.h"

#ifdef MOBILE
#include "deps/SDL2-2.0.8/include/SDL.h"
#else
#include "deps/SDL2/SDL.h"
#endif

static const uint8_t *KEYSTATE;

typedef struct TouchFinger      TouchFinger;
typedef struct Input            Input;

typedef enum MouseButtons
{
    MOUSE_LEFT = 0,
    MOUSE_RIGHT,
    MOUSE_MIDDLE,
    NUM_MOUSEBUTTONS
} MouseButtons;

#ifdef MOBILE
inline void
Input_calculateTouchPosition(Input *input, SDL_Event *event, Window *window);
#else
inline void
Input_calculateMousePosition(Input *input, Window *window, SDL_MouseButtonEvent button);
#endif

void
Input_update(Input *input, Window *window);

struct TouchFinger
{
    bool fingerDown;
    bool downThisFrame;
    bool upThisFrame;
    uvec2  lastPosition;
    uvec2  position;
};

struct Input
{
    unsigned int                fingerIndex;
    vec2                        trueInput;
    TouchFinger                 touchInputs[NUM_TOUCH_FINGERS];
    SDL_Keycode                 keyboardInput;

    void                        (*FingerDown)();
    void                        (*FingerUp)();
    void                        (*FingerMotion)();
    void                        (*KeyDown)();
    void                        (*KeyUp)();
};

