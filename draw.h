#pragma once

#include "defs.h"
#include "utils.h"
#include "window.h"

#ifdef MOBILE
#include "deps/SDL2-2.0.7/include/SDL.h"
#include "deps/SDL2_image-2.0.2/SDL_image.h"
#include "deps/GLES2/gl2.h"
#else
#include "deps/SDL2/SDL.h"
#include "deps/SDL2/SDL_image.h"
#include "deps/GL/glew.h"
#include "deps/SDL2/SDL_opengl.h"
#endif

typedef enum ShaderType
{
    SHADER_SPRITE = 0,
    SHADER_TRIANGLE = 1
} ShaderType;


typedef enum ShaderAttrLoc
{
    SHADER_ATTR_LOC_POS = 0,
    SHADER_ATTR_LOC_TEX,
    SHADER_ATTR_LOC_COL,

    NUM_SHADER_ATTR_LOCS
} ShaderAttrLoc;

typedef enum SpriteFlip
{
     SPRITE_FLIP_NONE,
     SPRITE_FLIP_HORIZONTAL,
     SPRITE_FLIP_VERTICAL,
     SPRITE_FLIP_BOTH
} SpriteFlip;

typedef struct Renderer             Renderer;
typedef struct Projection           Projection;
typedef struct Texture              Texture;
typedef struct Frame                Frame;
typedef struct Gradient             Gradient;
typedef struct Animation            Animation;
typedef struct BufferItem           BufferItem;
typedef struct Shader               Shader;
typedef struct DrawBatch            DrawBatch;

/* Return 0 on success */
int
Draw_init();

void
Draw_render(Renderer *renderer, Window *window);

Projection
Draw_prepRender(float *vp);

void
Draw_flush(float *vp, GLfloat projection[4][4]);

void
Draw_allocateMoreSprites();

void
Draw_allocateMoreTriangles();

void
Draw_dispose();

void
Draw_sprites(GLfloat projection[4][4]);

void
Draw_triangles(GLfloat projection[4][4]);

/* Note: setting clip to 0 is allowed to draw a full texture */
void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip);

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    SpriteFlip flip);

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    float scaleX, float scaleY);

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    float angle);

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    SpriteFlip flip, float scaleX, float scaleY);

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    SpriteFlip flip, float angle);

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    float scaleX, float scaleY, float angle);

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    SpriteFlip flip, float scaleX, float scaleY, float angle);

void
Draw_triangle(float x1, float y1, float x2, float y2, float x3, float y3);

void
Draw_triangle(float x1, float y1, float x2, float y2, float x3, float y3,
    unsigned int r, unsigned int g, unsigned int b, unsigned int a);

void
Draw_triangleGradient(float x1, float y1, float x2, float y2, float x3, float y3,
    SDL_Color c1, SDL_Color c2, SDL_Color c3, SDL_Color ca);

void
Draw_rectangle(float x, float y, float w, float h);

void
Draw_rectangle(float x, float y, float w, float h,
    unsigned int r, unsigned int g, unsigned int b, unsigned int a);

void
Draw_rectangleGradient(float x, float y, float w, float h,
    SDL_Color topLeft, SDL_Color topRight,
    SDL_Color botLeft, SDL_Color botRight,
    SDL_Color alpha);

int
Draw_createShader(Shader *shader);

void
Draw_loadTextureAndFreePrevious(Texture *texture,
     void *pixels, unsigned int w, unsigned int h,
     GLenum colorFormat, GLenum wrapMode);

int
Draw_loadTexture(Texture *texture, char *path);

void
Draw_freeTexture(Texture *texture);

struct Renderer
{
    void (*Render)();
    void (*PreRender)();
    void (*PostRender)();
};

struct Projection
{
    GLfloat view[4][4];
};

struct Texture
{
    GLuint          id;
    unsigned int    width;
    unsigned int    height;
};

struct Frame
{
    unsigned int rect[4]; // 0 = x, 1 = y, 2 = w, 3 = h
    unsigned int length;
};

struct Gradient
{
    GLuint id;
    unsigned int width;
    unsigned int height;
    float topR, topG, topB, topA;
    float botR, botG, botB, botA;
};

struct Animation
{
    unsigned int  frameCount;
    Frame         *frames;
};

struct BufferItem
{
    GLuint id;
    unsigned int repeats;
};

struct Shader
{
    GLuint program;
    GLint  projLoc;
    GLuint id;
    char *fragCode;
    char *vertCode;
};

struct DrawBatch
{
    GLuint              vbo;
    GLuint              ebo;

    Shader              *shaders;
    unsigned int        numShaders;

    GLfloat             *spriteVertexBuffer;
    unsigned int        spriteCount;

    BufferItem          *textureBuffer;
    unsigned int        textureBufferSize;
    unsigned int        texSwapCount;

    GLfloat             *triangleVertexBuffer;
    unsigned int        triangleBufferSize;
    unsigned int        triangleCount;

    unsigned int        gradientCount;
};
