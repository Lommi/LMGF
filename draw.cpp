#include "draw.h"

static DrawBatch drawBatch;

int
Draw_init()
{
    drawBatch.texSwapCount    = 0;
    drawBatch.spriteCount     = 0;
    drawBatch.triangleCount   = 0;

    drawBatch.shaders              = (Shader*)malloc(sizeof(Shader));
    drawBatch.spriteVertexBuffer   = (GLfloat*)malloc(BATCH_SIZE * FLOATS_PER_SPRITE * sizeof(GLfloat));
    drawBatch.textureBuffer        = (BufferItem*)malloc(BATCH_SIZE * sizeof(BufferItem));
    drawBatch.textureBufferSize    = BATCH_SIZE;

    drawBatch.triangleVertexBuffer = (GLfloat*)malloc(BATCH_SIZE * FLOATS_PER_TRIANGLE * sizeof(GLfloat));
    drawBatch.triangleBufferSize   = BATCH_SIZE;

    Shader spriteShader;
    spriteShader.fragCode = SHADER_SPRITE_FRAG;
    spriteShader.vertCode = SHADER_SPRITE_VERT;

    Shader triangleShader;
    triangleShader.fragCode = SHADER_GEOMETRY_FRAG;
    triangleShader.vertCode = SHADER_GEOMETRY_VERT;

    Draw_createShader(&spriteShader);
    Draw_createShader(&triangleShader);

    glGenBuffers(1, &drawBatch.vbo);
    glGenBuffers(1, &drawBatch.ebo);

    return 0;
};

void
Draw_render(Renderer *renderer, Window *window)
{
    glViewport(0, 0, window->width, window->height);
    glClearColor(window->r, window->g, window->b, window->a);
    glClear(GL_COLOR_BUFFER_BIT);

    window->viewport[2] = window->viewport[0] + window->resolution.x * window->viewportScale;
    window->viewport[3] = window->viewport[1] + window->resolution.y * window->viewportScale;

    auto projection = Draw_prepRender(window->viewport);

    if (renderer->PreRender)
    {
        renderer->PreRender();
        Draw_flush(window->viewport, projection.view);
    }

    if (renderer->Render)
    {
        renderer->Render();
        Draw_flush(window->viewport, projection.view);
    }

    if (renderer->PostRender)
    {
        renderer->PostRender();
        Draw_flush(window->viewport, projection.view);
    }

    SDL_GL_SwapWindow(window->sdlWindow);
}

void
Draw_allocateMoreSprites()
{
    unsigned int newSize = (unsigned int)((float)drawBatch.textureBufferSize * 1.05f);

    if (newSize <= drawBatch.textureBufferSize)
    {
        newSize = drawBatch.textureBufferSize + 1;
    }

    void *vertexBuf = malloc(newSize * FLOATS_PER_SPRITE * sizeof(GLfloat));
    void *newBuf    = malloc(newSize * FLOATS_PER_SPRITE * sizeof(BufferItem));

    memcpy(vertexBuf, drawBatch.spriteVertexBuffer,
        drawBatch.textureBufferSize * FLOATS_PER_SPRITE * sizeof(GLfloat));
    memcpy(newBuf, drawBatch.textureBuffer,
        drawBatch.textureBufferSize * sizeof(BufferItem));

    free(drawBatch.spriteVertexBuffer);
    free(drawBatch.textureBuffer);

    drawBatch.spriteVertexBuffer = (GLfloat*)vertexBuf;
    drawBatch.textureBuffer      = (BufferItem*)newBuf;
    drawBatch.textureBufferSize  = newSize;
}

void
Draw_allocateMoreTriangles()
{
    unsigned int newSize = (unsigned int)((float)drawBatch.triangleBufferSize * 1.05f);

    if (newSize <= drawBatch.triangleBufferSize)
    {
        newSize = drawBatch.triangleBufferSize + 1;
    }

    void *triangleBuf = malloc(newSize * FLOATS_PER_TRIANGLE * sizeof(GLfloat));

    memcpy(triangleBuf, drawBatch.triangleVertexBuffer,
        drawBatch.triangleBufferSize * FLOATS_PER_TRIANGLE * sizeof(GLfloat));

    free(drawBatch.triangleVertexBuffer);

    drawBatch.triangleVertexBuffer = (GLfloat*)triangleBuf;
    drawBatch.triangleBufferSize   = newSize;
}

void
Draw_dispose()
{
    free(drawBatch.spriteVertexBuffer);
    free(drawBatch.textureBuffer);
    free(drawBatch.triangleVertexBuffer);
    glDeleteBuffers(1, &drawBatch.vbo);
    glDeleteBuffers(1, &drawBatch.ebo);
    for (unsigned int i = 0; i < drawBatch.numShaders; ++i)
    {
        glDeleteProgram(drawBatch.shaders[i].program);
    }
}

Projection
Draw_prepRender(float *vp)
{
    /* Set up the projection matrix by screen dimensions */
    GLint viewport[4];

    if (vp)
    {
        viewport[0] = (GLint)vp[0];
        viewport[1] = (GLint)vp[1];
        viewport[2] = (GLint)vp[2];
        viewport[3] = (GLint)vp[3];
    }
    else
    {
        glGetIntegerv(GL_VIEWPORT, viewport);
    }

    Projection projection;

    projection.view[0][0] = 2.0f / (GLfloat)(viewport[2] - viewport[0]);
    projection.view[0][1] = 0.0f;
    projection.view[0][2] = 0.0f;
    projection.view[0][3] = 0.0f;

    projection.view[1][0] = 0.0f;
    projection.view[1][1] = 2.0f / (GLfloat)(viewport[1] - viewport[3]);
    projection.view[1][2] = 0.0f;
    projection.view[1][3] = 0.0f;

    projection.view[2][0] = 0.0f;
    projection.view[2][1] = 0.0f;
    projection.view[2][2] = -1.0f;
    projection.view[2][3] = 0.0f;

    projection.view[3][0] = -(GLfloat)(viewport[2] + viewport[0]) / (GLfloat)(viewport[2] - viewport[0]);
    projection.view[3][1] = -(GLfloat)(viewport[1] + viewport[3]) / (GLfloat)(viewport[1] - viewport[3]);
    projection.view[3][2] = 0.0f;
    projection.view[3][3] = 1.0f;

    return projection;
}

void
Draw_flush(float *vp, GLfloat projection[4][4])
{
    if (drawBatch.spriteCount < 1 && drawBatch.triangleCount < 1)
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        return;
    }

    Draw_sprites(projection);
    Draw_triangles(projection);

    drawBatch.texSwapCount = 0;
    drawBatch.spriteCount = 0;
    drawBatch.triangleCount = 0;
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void
Draw_sprites(GLfloat projection[4][4])
{
    if (drawBatch.spriteCount < 1)
        return;

    Shader *shader = &drawBatch.shaders[SHADER_SPRITE];
    if (!shader)
    {
        PRINT("Draw_flush: SHADER_SPRITE\n");
        return;
    }

    glUseProgram(shader->program);

    glUniformMatrix4fv(shader->projLoc, 1, GL_FALSE, &projection[0][0]);

    // Buffer data
    unsigned int numBatches = 0;
    unsigned int batchElementsDrawn = 0;
    unsigned int elementBufOffset = 0;
    unsigned int elementsInBatch = 0;
    unsigned int repeats;
    GLuint lastElement;
    GLuint curElement;

    glBindBuffer(GL_ARRAY_BUFFER, drawBatch.vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, drawBatch.ebo);

    GLushort indices[6] = {0, 1, 2, 2, 3, 1};
    GLushort *indiceData = (GLushort*)malloc(BATCH_SIZE * 6 * sizeof(GLushort));
    for (GLushort i = 0; i < BATCH_SIZE; ++i)
        for (GLushort j = 0; j < 6; ++j)
            indiceData[i * 6 + j] = indices[j] + i * 4;
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 BATCH_SIZE * 6 * sizeof(GLushort),
                 indiceData,
                 GL_DYNAMIC_DRAW);
    free(indiceData);

    shader->projLoc = glGetUniformLocation(shader->program, "p");

    glBufferData(GL_ARRAY_BUFFER,
        drawBatch.textureBufferSize * FLOATS_PER_SPRITE * sizeof(GLfloat),
        drawBatch.spriteVertexBuffer, GL_DYNAMIC_DRAW);

    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 2, GL_FLOAT, GL_FALSE,
        4 * sizeof(GLfloat), (const GLvoid*)0);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);

    glVertexAttribPointer(SHADER_ATTR_LOC_TEX, 2, GL_FLOAT, GL_FALSE,
        4 * sizeof(GLfloat), (const GLvoid*)(2 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_TEX);

    glBindBuffer(GL_ARRAY_BUFFER, drawBatch.vbo);

    glActiveTexture(GL_TEXTURE0);
    lastElement = drawBatch.textureBuffer[0].id;
    glBindTexture(GL_TEXTURE_2D, lastElement);

    numBatches = drawBatch.spriteCount / BATCH_SIZE + 1;

    for (unsigned int batchNum = 0; batchNum < numBatches; ++batchNum)
    {
        elementsInBatch = drawBatch.spriteCount - batchNum * BATCH_SIZE;
        if (elementsInBatch > BATCH_SIZE) elementsInBatch = BATCH_SIZE;

        batchElementsDrawn = 0;

        glBufferSubData(GL_ARRAY_BUFFER,                                                  // Target
                        0,                                                                // Offset
                        elementsInBatch * FLOATS_PER_SPRITE * sizeof(GLfloat),          // Size
                        &drawBatch.spriteVertexBuffer[batchNum * BATCH_SIZE * FLOATS_PER_SPRITE]);   // Data

        while (batchElementsDrawn < elementsInBatch)
        {
            // Bind the correct texture
            curElement = drawBatch.textureBuffer[elementBufOffset].id;

            if (curElement  != lastElement)
            {
                glBindTexture(GL_TEXTURE_2D, drawBatch.textureBuffer[elementBufOffset].id);
                lastElement = curElement;
            }

            // Draw the amount of repeats suggested by the texture buffer item
            repeats = drawBatch.textureBuffer[elementBufOffset].repeats + 1;

            if (batchElementsDrawn + repeats > elementsInBatch)
            {
                repeats = elementsInBatch - batchElementsDrawn;
                drawBatch.textureBuffer[elementBufOffset].repeats -= repeats;
            }
            else
            {
                ++elementBufOffset;
            }
            glDrawElements(GL_TRIANGLES,        // mode
                           repeats * 6,		    // count
                           GL_UNSIGNED_SHORT,	// type
                           (GLvoid*)(batchElementsDrawn * 6 * sizeof(GLushort))); // indices
            batchElementsDrawn += repeats;
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void
Draw_triangles(GLfloat projection[4][4])
{
    if (drawBatch.triangleCount < 1)
        return;

    Shader *shader = &drawBatch.shaders[SHADER_TRIANGLE];
    if (!shader)
    {
        PRINT("Draw_flush: SHADER_TRIANGLE\n");
        return;
    }

    glUseProgram(shader->program);

    glUniformMatrix4fv(shader->projLoc, 1, GL_FALSE, &projection[0][0]);

    glBindBuffer(GL_ARRAY_BUFFER, drawBatch.vbo);

    shader->projLoc  = glGetUniformLocation(shader->program, "p");

    glBufferData(GL_ARRAY_BUFFER,
        drawBatch.triangleBufferSize * FLOATS_PER_TRIANGLE * sizeof(GLfloat),
        drawBatch.triangleVertexBuffer,
        GL_DYNAMIC_DRAW);

    glVertexAttribPointer(SHADER_ATTR_LOC_POS, 2, GL_FLOAT, GL_FALSE,
        3 * sizeof(GLfloat), (const GLvoid*)0);
    glEnableVertexAttribArray(SHADER_ATTR_LOC_POS);

    glVertexAttribPointer(SHADER_ATTR_LOC_COL, 4, GL_UNSIGNED_BYTE, GL_TRUE,
        3 * sizeof(GLfloat), (const GLvoid*)(2 * sizeof(GLfloat)));
    glEnableVertexAttribArray(SHADER_ATTR_LOC_COL);

    glDrawArrays(GL_TRIANGLES, 0, 3 * drawBatch.triangleCount);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip)
{
    if (texture == NULL)
    {
        PRINT("Draw_sprite(): Texture == NULL. Can't draw\n");
        return;
    }

    if (drawBatch.spriteCount + 1 > drawBatch.textureBufferSize)
        Draw_allocateMoreSprites();

    unsigned int index = drawBatch.spriteCount * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION();
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS();
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION();
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS();
    }

    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    SpriteFlip flip)
{
    if (texture == NULL)
    {
        PRINT("Draw_sprite(): Texture == NULL. Can't draw\n");
        return;
    }

    if (drawBatch.spriteCount + 1 > drawBatch.textureBufferSize)
        Draw_allocateMoreSprites();

    unsigned int index = drawBatch.spriteCount * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION();
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS();
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION();
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS();
    }

    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    float scaleX, float scaleY)
{
    if (texture == NULL)
    {
        PRINT("Draw_sprite(): Texture == NULL. Can't draw\n");
        return;
    }

    if (drawBatch.spriteCount + 1 > drawBatch.textureBufferSize)
        Draw_allocateMoreSprites();

    unsigned int index = drawBatch.spriteCount * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION();
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS();
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION();
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS();
    }

    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    float angle)
{
    if (texture == NULL)
    {
        PRINT("Draw_sprite(): Texture == NULL. Can't draw\n");
        return;
    }

    if (drawBatch.spriteCount + 1 > drawBatch.textureBufferSize)
        Draw_allocateMoreSprites();

    unsigned int index = drawBatch.spriteCount * FLOATS_PER_SPRITE;

    GLfloat originX;
    GLfloat originY;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION();
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS();
        originX = ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        originY = ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION();
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS();
        originX = ((GLfloat)x + (GLfloat)texture->width/2.0f);
        originY = ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE();

    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    SpriteFlip flip, float scaleX, float scaleY)
{
    if (texture == NULL)
    {
        PRINT("Draw_sprite(): Texture == NULL. Can't draw\n");
        return;
    }

    if (drawBatch.spriteCount + 1 > drawBatch.textureBufferSize)
        Draw_allocateMoreSprites();

    unsigned int index = drawBatch.spriteCount * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION();
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS();
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION();
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS();
    }

    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    SpriteFlip flip, float angle)
{
    if (texture == NULL)
    {
        PRINT("Draw_sprite(): Texture == NULL. Can't draw\n");
        return;
    }

    if (drawBatch.spriteCount + 1 > drawBatch.textureBufferSize)
        Draw_allocateMoreSprites();

    unsigned int index = drawBatch.spriteCount * FLOATS_PER_SPRITE;

    GLfloat originX;
    GLfloat originY;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION();
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS();
        originX = ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        originY = ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION();
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS();
        originX = ((GLfloat)x + (GLfloat)texture->width/2.0f);
        originY = ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE();
    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    float scaleX, float scaleY, float angle)
{
    if (texture == NULL)
    {
        PRINT("Draw_sprite(): Texture == NULL. Can't draw\n");
        return;
    }

    if (drawBatch.spriteCount + 1 > drawBatch.textureBufferSize)
        Draw_allocateMoreSprites();

    unsigned int index = drawBatch.spriteCount * FLOATS_PER_SPRITE;

    GLfloat originX;
    GLfloat originY;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION();
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS();
        originX = ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        originY = ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION();
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS();
        originX = ((GLfloat)x + (GLfloat)texture->width/2.0f);
        originY = ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE();
    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
Draw_sprite(Texture *texture, float x, float y, unsigned int *clip,
    SpriteFlip flip, float scaleX, float scaleY, float angle)
{
    if (texture == NULL)
    {
        PRINT("Draw_sprite(): Texture == NULL. Can't draw\n");
        return;
    }

    if (drawBatch.spriteCount + 1 > drawBatch.textureBufferSize)
        Draw_allocateMoreSprites();

    unsigned int index = drawBatch.spriteCount * FLOATS_PER_SPRITE;

    GLfloat originX;
    GLfloat originY;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION();
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS();
        originX = ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        originY = ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION();
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS();
        originX = ((GLfloat)x + (GLfloat)texture->width/2.0f);
        originY = ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE();
    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
Draw_triangle(float x1, float y1, float x2, float y2, float x3, float y3)
{
    if (drawBatch.triangleCount + 1 > drawBatch.triangleBufferSize)
        Draw_allocateMoreTriangles();

    unsigned int index = drawBatch.triangleCount * FLOATS_PER_TRIANGLE;
    unsigned int r = 255, g = 255, b = 255, a = 255;

    SET_TRIANGLE();

    ++drawBatch.triangleCount;
}

void
Draw_triangle(float x1, float y1, float x2, float y2, float x3, float y3,
    unsigned int r, unsigned int g, unsigned int b, unsigned int a)
{
    if (drawBatch.triangleCount + 1 > drawBatch.triangleBufferSize)
        Draw_allocateMoreTriangles();

    unsigned int index = drawBatch.triangleCount * FLOATS_PER_TRIANGLE;

    SET_TRIANGLE();

    ++drawBatch.triangleCount;
}

void
Draw_triangleGradient(float x1, float y1, float x2, float y2, float x3, float y3,
    SDL_Color c1, SDL_Color c2, SDL_Color c3, SDL_Color ca)
{
    if (drawBatch.triangleCount + 1 > drawBatch.triangleBufferSize)
        Draw_allocateMoreTriangles();

    unsigned int index = drawBatch.triangleCount * FLOATS_PER_TRIANGLE;

    SET_TRIANGLE_GRADIENT();

    ++drawBatch.triangleCount;
}

void
Draw_rectangle(float x, float y, float w, float h)
{
    Draw_triangle(x, y, x, y + h, x + w, y);
    Draw_triangle(x, y + h, x + w, y + h, x + w, y);
}

void
Draw_rectangle(float x, float y, float w, float h,
    unsigned int r, unsigned int g, unsigned int b, unsigned int a)
{
    Draw_triangle(x, y, x, y + h, x + w, y, r, g, b, a);
    Draw_triangle(x, y + h, x + w, y + h, x + w, y, r, g, b, a);
}

void
Draw_rectangleGradient(float x, float y, float w, float h,
    SDL_Color topLeft, SDL_Color topRight,
    SDL_Color botLeft, SDL_Color botRight,
    SDL_Color alpha)
{
    Draw_triangleGradient(x, y, x, y + h, x + w, y, topLeft, botLeft, topRight, alpha);
    Draw_triangleGradient(x, y + h, x + w, y + h, x + w, y, botLeft, botRight, topRight, alpha);
}

void
Draw_freeTexture(Texture *texture)
{
    if (texture->id != 0)
    {
        glDeleteTextures(1, &texture->id);
        texture->id = 0;
    }

    texture->width	= 0;
    texture->height	= 0;
}

int
Draw_createShader(Shader *shader)
{
    if (!shader)
    {
        PRINT("Draw_createShader shader null\n");
        return 1;
    }
    if (!shader->vertCode)
    {
        PRINT("Draw_createShader vertCode null\n");
        return 2;
    }
    if (!shader->fragCode)
    {
        PRINT("Draw_createShader fragCode null\n");
        return 3;
    }

    shader->id = drawBatch.numShaders;

    const char *fragCode = shader->fragCode;
    const char *vertCode = shader->vertCode;

    GLuint vtxShader = glCreateShader(GL_VERTEX_SHADER);
    GLuint frgShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(vtxShader, 1, &vertCode, 0);
    glShaderSource(frgShader, 1, &fragCode, 0);
    glCompileShader(vtxShader);
    glCompileShader(frgShader);

    GLint success;

    const unsigned int log_len = 512;
    GLchar errorLog[log_len];

    glGetShaderiv(vtxShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vtxShader, log_len, NULL, errorLog);
        PRINT("vtxShader compile error: %s\n", errorLog);
        glDeleteShader(vtxShader);
        glDeleteShader(frgShader);
        exit(0);
    }

    glGetShaderiv(frgShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(frgShader, log_len, NULL, errorLog);
        PRINT("frgShader compile error: %s\n", errorLog);
        glDeleteShader(vtxShader);
        glDeleteShader(frgShader);
        exit(0);
    }

    GLuint program = glCreateProgram();

    glAttachShader(program, vtxShader);
    glAttachShader(program, frgShader);

    glBindAttribLocation(program, SHADER_ATTR_LOC_POS, "pos");
    glBindAttribLocation(program, SHADER_ATTR_LOC_TEX, "inTexCoord");
    glBindAttribLocation(program, SHADER_ATTR_LOC_COL, "inColor");

    glLinkProgram(program);
    glDeleteShader(vtxShader);
    glDeleteShader(frgShader);

    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(program, log_len, NULL, errorLog);
        PRINT("Shader program compile error: %s\n", errorLog);
        glDeleteProgram(program);
        exit(0);
    }

    shader->program = program;

    drawBatch.shaders = (Shader*)realloc(drawBatch.shaders, (drawBatch.numShaders + 1) * sizeof(Shader));
    drawBatch.shaders[drawBatch.numShaders] = *shader;
    ++drawBatch.numShaders;

    return 0;
}

void
Draw_loadTextureAndFreePrevious(Texture *texture, void *pixels,
    unsigned int w, unsigned int h,
    GLenum colorFormat,
    GLenum wrapMode)
{
    if (texture->id != 0)
        Draw_freeTexture(texture);

    texture->width  = w;
    texture->height = h;

    glGenTextures(1, &texture->id);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);

    glTexImage2D(GL_TEXTURE_2D, 0, colorFormat,
                 texture->width, texture->height,
                 0, colorFormat, GL_UNSIGNED_BYTE,
                 pixels);

    glBindTexture(GL_TEXTURE_2D, 0);
}

int
Draw_loadTexture(Texture *texture, char *path)
{
    const char *fullpath = AppendString(ASSETS_PATH, path);
	SDL_Surface *surface = IMG_Load(fullpath);

    if (!surface)
    {
        PRINT("IMG_Load() failure: %s\n", IMG_GetError());
        return 1;
    }

    if (surface->format->BytesPerPixel != 4)
    {
        SDL_Surface *csurface = SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_RGBA8888, 0);
        SDL_FreeSurface(surface);
        surface = csurface;
    }

    glGenTextures(1, &texture->id);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);

    glBindTexture(GL_TEXTURE_2D, 0);

    texture->width  = surface->w;
    texture->height = surface->h;

    SDL_FreeSurface(surface);

    return 0;
}
